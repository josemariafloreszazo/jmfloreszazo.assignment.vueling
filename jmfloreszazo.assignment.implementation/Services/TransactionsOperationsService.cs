﻿using System;
using System.Collections.Generic;
using System.Linq;
using jmfloreszazo.assignment.contracts.Model;

namespace jmfloreszazo.assignment.implementation.Services
{
    public class TransactionsOperationsService : ITransactionsOperationsService
    {
        private readonly CurrencyConvertsService _currencyConvertsService;

        public TransactionsOperationsService()
        {
            _currencyConvertsService = new CurrencyConvertsService();

        }

        public TransactionsModel Get(string sku, RatesModel rate, List<TransactionsModel> transactionsModel)
        {
            var findTransaction = transactionsModel.SingleOrDefault(o => o.Sku == sku);
            return ConvertTransactionsModel(rate, findTransaction);
        }

        public List<TransactionsModel> Get(RatesModel rate, List<TransactionsModel> transactionsModel)
        {
            var result = new List<TransactionsModel>();
            foreach (var transactions in transactionsModel)
            {
                var convertTransactions = ConvertTransactionsModel(rate, transactions);
                result.Add(convertTransactions);
            }
            return result;
        }

        private TransactionsModel ConvertTransactionsModel(RatesModel rate, TransactionsModel transactionsModel)
        {
            var result = new TransactionsModel();
            decimal totalAmount = 0.0m;
            foreach (var transaction in transactionsModel.Detail)
            {
                if (rate.From == transaction.Currency)
                {
                    totalAmount += _currencyConvertsService.toValue(transaction.Amount);
                }
                else
                {
                    var rateValue = rate.Rates.SingleOrDefault(o => o.To == transaction.Currency)?.Rate;
                    totalAmount += _currencyConvertsService.toValue(transaction.Amount) * _currencyConvertsService.toValue(rateValue);
                }
            }
            result.Detail = transactionsModel.Detail;
            result.Sku = transactionsModel.Sku;
            result.TotalAmount = _currencyConvertsService.toValue(totalAmount, 2);
            return result;
        }
    }
}
