﻿using System.Collections.Generic;
using jmfloreszazo.assignment.contracts.Model;

namespace jmfloreszazo.assignment.implementation.Services
{
    public interface ITransactionsOperationsService
    {
        TransactionsModel Get(string sku, RatesModel rate, List<TransactionsModel> transactionsModel); // This is a proof of concept, but it should be a constrain with generic type
        List<TransactionsModel> Get(RatesModel rate, List<TransactionsModel> transactionsModel);
    }
}