﻿using System;
using System.Collections.Generic;
using System.Text;

namespace jmfloreszazo.assignment.implementation.Services
{
    public interface ICurrencyConvertsService
    {
        decimal toValue(string fromValue);
        string toValue(decimal fromValue, int round);
    }
}
