﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;
using jmfloreszazo.assignment.contracts.Model;
using jmfloreszazo.assignment.contracts.Services;
using Newtonsoft.Json;

namespace jmfloreszazo.assignment.implementation.Services
{
    public class RatesService : IRatesService
    {
        private readonly CurrencyConvertsService _currencyConvertsService;

        public RatesService()
        {
            _currencyConvertsService = new CurrencyConvertsService();
        }

        public List<RatesResponseModel> GetRawData(string uri)
        {
            var json = (new WebClient()).DownloadString(uri);
            var rates = JsonConvert.DeserializeObject<List<RatesResponseModel>>(json);
            return rates;
        }

        public List<RatesModel> GetNormalizeData(List<RatesResponseModel> ratesResponseModel)
        {
            var normalizeData = new List<RatesModel>();
            foreach (var rate in ratesResponseModel.GroupBy(o => o.From))
            {
                normalizeData.Add(new RatesModel(){From = rate.Key});
            }
            foreach (var rate in ratesResponseModel.GroupBy(o => o.To))
            {
                var result = normalizeData.First(o => o.From == rate.Key).From;
                if (result.Length==0)
                {
                    normalizeData.Add(new RatesModel() { From = result });
                }
            }
            foreach (var rate in normalizeData)
            {
                var result = normalizeData.Except(new List<RatesModel>{rate});
                var to = new List<RateModel>();
                foreach (var item in result)
                {
                    to.Add(new RateModel(){To = item.From});
                }
                rate.Rates = to;
            }
            foreach (var rate in normalizeData)
            {
                foreach (var item in rate.Rates)
                {
                    var result =  ratesResponseModel.SingleOrDefault(o => o.From == rate.From && o.To == item.To);
                    if (result != null)
                    {
                        item.Rate = result.Rate;
                    }
                }
            }
            foreach (var rate in normalizeData)
            {
                if (rate.Rates.Where(o => string.IsNullOrEmpty(o.Rate)).Any())
                {
                    foreach (var item in rate.Rates)
                    {
                        if (string.IsNullOrEmpty(item.Rate))
                        {
                            var oppositeToExist = normalizeData.SelectMany(o => o.Rates.Where(so =>
                                so.To == rate.From && !string.IsNullOrEmpty(so.Rate) && o.From == item.To));
                            if (oppositeToExist.Any())
                            {
                                var oppositeTo = oppositeToExist.ToList();
                                var result = _currencyConvertsService.toValue(oppositeTo?[0].Rate) * 1;
                                item.Rate = _currencyConvertsService.toValue(result, 2);
                            }
                            else
                            {
                                var findItems = rate.Rates.Where(o => o.To != item.To && o.To != rate.From);
                                foreach (var oItem in findItems )
                                {
                                    if (string.IsNullOrEmpty(item.Rate))
                                    {
                                        var indirectRate = normalizeData.SingleOrDefault(o => o.From == oItem.To)?.Rates.Where(os => os.To == item.To).ToList();
                                        var convertRate = normalizeData.SingleOrDefault(o => o.From == rate.From)?.Rates.Where(os => os.To == oItem.To).ToList();
                                        if (string.IsNullOrEmpty(indirectRate?[0].Rate) ||
                                            string.IsNullOrEmpty(convertRate?[0].Rate))
                                        {
                                            item.Rate = _currencyConvertsService.toValue(0.0m, 3);
                                        }
                                        else
                                        {
                                            var result = _currencyConvertsService.toValue(indirectRate?[0].Rate) * _currencyConvertsService.toValue(convertRate?[0].Rate);
                                            item.Rate = _currencyConvertsService.toValue(result, 3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return normalizeData;
        }
    }
}
