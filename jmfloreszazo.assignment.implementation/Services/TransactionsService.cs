﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using jmfloreszazo.assignment.contracts.Model;
using jmfloreszazo.assignment.contracts.Services;
using Newtonsoft.Json;

namespace jmfloreszazo.assignment.implementation.Services
{
    public class TransactionsService : ITransactionsService
    {
        public List<TransactionsResponseModel> GetRawData(string uri)
        {
            var json = (new WebClient()).DownloadString(uri);
            var transaction = JsonConvert.DeserializeObject<List<TransactionsResponseModel>>(json);
            return transaction;
        }

        public List<TransactionsModel> GetNormalizeData(List<TransactionsResponseModel> transactionResponseModel)
        {
            var normalizeData = new List<TransactionsModel>();
            foreach (var transactions in transactionResponseModel.GroupBy(o => o.Sku))
            {
                var detail = new List<TransactionsDetailModel>();
                foreach (var items in transactions)
                {
                    detail.Add(new TransactionsDetailModel()
                    {
                        Amount = items.Amount,
                        Currency = items.Currency
                    });
                }
                normalizeData.Add(new TransactionsModel()
                {
                    Sku = transactions.Key,
                    TotalAmount = string.Empty,
                    Detail = detail
                });
            }
            return normalizeData;
        }
    }
}
