﻿using System;
using System.Globalization;

namespace jmfloreszazo.assignment.implementation.Services
{
    public class CurrencyConvertsService : ICurrencyConvertsService
    {
        public decimal toValue(string fromValue)
        {
            return decimal.Parse(fromValue, CultureInfo.InvariantCulture);
        }

        public string toValue(decimal fromValue, int round)
        {
            var roundValue = decimal.Round(fromValue, round, MidpointRounding.AwayFromZero);
            return roundValue.ToString(CultureInfo.InvariantCulture);
        }
    }
}