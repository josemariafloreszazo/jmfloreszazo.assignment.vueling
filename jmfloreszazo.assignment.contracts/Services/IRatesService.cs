﻿using System.Collections.Generic;
using jmfloreszazo.assignment.contracts.Model;

namespace jmfloreszazo.assignment.contracts.Services
{
    public interface IRatesService
    {
        List<RatesResponseModel> GetRawData(string uri);
        List<RatesModel> GetNormalizeData(List<RatesResponseModel> ratesResponseModel);
    }
}
