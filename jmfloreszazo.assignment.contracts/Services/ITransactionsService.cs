﻿using System.Collections.Generic;
using jmfloreszazo.assignment.contracts.Model;

namespace jmfloreszazo.assignment.contracts.Services
{
    public interface ITransactionsService
    {
        List<TransactionsResponseModel> GetRawData(string uri);
        List<TransactionsModel> GetNormalizeData(List<TransactionsResponseModel> transactionResponseModel);
    }
}
