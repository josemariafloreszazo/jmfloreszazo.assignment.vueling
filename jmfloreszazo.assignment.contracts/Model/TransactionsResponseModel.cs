﻿using Newtonsoft.Json;

namespace jmfloreszazo.assignment.contracts.Model
{
    public class TransactionsResponseModel
    {
        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
