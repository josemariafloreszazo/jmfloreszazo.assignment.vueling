﻿using Newtonsoft.Json;

namespace jmfloreszazo.assignment.contracts.Model
{
    public class RatesResponseModel
    {
        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("rate")]
        public string Rate { get; set; }
    }
}
