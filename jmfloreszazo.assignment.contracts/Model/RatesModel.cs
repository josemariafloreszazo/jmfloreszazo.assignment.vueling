﻿using System.Collections.Generic;

namespace jmfloreszazo.assignment.contracts.Model
{
    public class RatesModel
    {
        public string From { get; set; }
        public List<RateModel> Rates { get; set; }

    }
}
