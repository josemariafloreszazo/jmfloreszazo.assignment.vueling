﻿using System.Collections.Generic;

namespace jmfloreszazo.assignment.contracts.Model
{
    public class TransactionsModel
    {
        public string Sku { get; set; }
        public virtual string TotalAmount { get; set; }
        public List<TransactionsDetailModel> Detail { get; set; }
    }
}
