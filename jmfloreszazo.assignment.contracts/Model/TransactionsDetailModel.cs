﻿namespace jmfloreszazo.assignment.contracts.Model
{
    public class TransactionsDetailModel
    {
        public string Amount { get; set; }
        public string Currency { get; set; }
    }
}
