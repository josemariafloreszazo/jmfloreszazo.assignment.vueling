using jmfloreszazo.assignment.implementation.Services;
using System.Linq;
using Xunit;

namespace jmfloreszazo.assignment.unittest
{
    public class When_TransactionsOperationsService_IsCalled
    {
        // In this case the best option is use mock for: uri, object, etc.  

        private const string RatesURI = "http://quiet-stone-2094.herokuapp.com/rates.json";
        private const string TrasnactionsURI = "http://quiet-stone-2094.herokuapp.com/transactions.json";

        private readonly RatesService _ratesService;
        private readonly TransactionsService _transactionService;
        private readonly TransactionsOperationsService _transactionsOperationsService;

        public When_TransactionsOperationsService_IsCalled()
        {
            _ratesService = new RatesService();
            _transactionService = new TransactionsService();
            _transactionsOperationsService = new TransactionsOperationsService();
        }

        [Fact]
        public void When_TransactionsOperationsService_WithRate_CanGetAllTransaction()
        {
            var rawRatesResult = _ratesService.GetRawData(RatesURI);
            Assert.True(rawRatesResult.Count > 0);
            var rawTrasnactionsResult = _transactionService.GetRawData(TrasnactionsURI);
            Assert.True(rawTrasnactionsResult.Count>0);
            var normalizeRatesResult = _ratesService.GetNormalizeData(rawRatesResult);
            Assert.True(normalizeRatesResult.Count > 0);
            var normalizeTrasnactionsResult = _transactionService.GetNormalizeData(rawTrasnactionsResult);
            Assert.True(normalizeTrasnactionsResult.Count > 0);
            var result =
                _transactionsOperationsService.Get(normalizeTrasnactionsResult[0].Sku, normalizeRatesResult[0], normalizeTrasnactionsResult);
            Assert.True(result.TotalAmount.Length>0);
        }

        [Fact]
        public void When_TransactionsOperationsService_WithRate_CanGetFilterTransaction()
        {
            var rawRatesResult = _ratesService.GetRawData(RatesURI);
            Assert.True(rawRatesResult.Count > 0);
            var rawTrasnactionsResult = _transactionService.GetRawData(TrasnactionsURI);
            Assert.True(rawTrasnactionsResult.Count > 0);
            var normalizeRatesResult = _ratesService.GetNormalizeData(rawRatesResult);
            Assert.True(normalizeRatesResult.Count > 0);
            var normalizeTrasnactionsResult = _transactionService.GetNormalizeData(rawTrasnactionsResult);
            Assert.True(normalizeTrasnactionsResult.Count > 0);
            var result =
                _transactionsOperationsService.Get(normalizeRatesResult[0], normalizeTrasnactionsResult);
            Assert.True(result.Count > 0);
        }

    }
}
