using jmfloreszazo.assignment.implementation.Services;
using System.Linq;
using Xunit;

namespace jmfloreszazo.assignment.unittest
{
    public class When_TransactionsService_IsCalled
    {
        // Injecting the configuration we could treat it as an integration test

        private const string TrasnactionsURI = "http://quiet-stone-2094.herokuapp.com/transactions.json";

        private readonly TransactionsService _transactionService;

        public When_TransactionsService_IsCalled()
        {
            _transactionService = new TransactionsService();
        }

        [Fact]
        public void Given_TransactionsService_AtLeastHaveData()
        {
            var rawResult = _transactionService.GetRawData(TrasnactionsURI);
            Assert.True(rawResult.Count>0);
        }

        [Fact]
        public void Given_TransactionsService_CanNormalizeData()
        {
            var rawResult = _transactionService.GetRawData(TrasnactionsURI);

            Assert.True(rawResult.Count > 0);

            var normalizeResult = _transactionService.GetNormalizeData(rawResult);

            Assert.True(normalizeResult.Count > 0);
        }
    }
}
