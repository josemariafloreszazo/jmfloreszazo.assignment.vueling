using jmfloreszazo.assignment.implementation.Services;
using System;
using Xunit;

namespace jmfloreszazo.assignment.unittest
{
    public class When_CurrencyConvertsService_IsCalled
    {
        private readonly CurrencyConvertsService _currencyConvertsService;

        public When_CurrencyConvertsService_IsCalled()
        {
            _currencyConvertsService = new CurrencyConvertsService();
        }

        [Fact]
        public void Given_CurrencyConvertsService_ConvertStringToDecimal()
        {
            decimal compareDecimal = 10.91m;
            var result = _currencyConvertsService.toValue("10.91");
            Assert.Equal(compareDecimal, result);
        }

        [Fact]
        public void Given_CurrencyConvertsService_ConvertDecimalToString()
        {
            string compareDecimal = "10.91";
            var result = _currencyConvertsService.toValue(10.9121m, 2);
            Assert.Equal(compareDecimal , result);
        }
    }
}
