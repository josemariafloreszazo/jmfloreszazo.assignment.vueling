using jmfloreszazo.assignment.implementation.Services;
using System.Linq;
using Xunit;

namespace jmfloreszazo.assignment.unittest
{
    public class When_RatesService_IsCalled
    {
        // Injecting the configuration we could treat it as an integration test

        private const string RatesURI = "http://quiet-stone-2094.herokuapp.com/rates.json";

        private readonly RatesService _ratesService;

        public When_RatesService_IsCalled()
        {
            _ratesService = new RatesService();
        }

        [Fact]
        public void Given_RatesService_AtLeastHaveData()
        {
            var rawResult = _ratesService.GetRawData(RatesURI);

            Assert.True(rawResult.Count>0);
        }

        [Fact]
        public void Given_RatesService_CanNormalizeData()
        {
            var rawResult = _ratesService.GetRawData(RatesURI);

            Assert.True(rawResult.Count > 0);

            var normalizeResult = _ratesService.GetNormalizeData(rawResult);

            Assert.True(normalizeResult.Count > 0);
        }

        [Fact]
        public void Given_RatesService_WithNormalizeDataNoneCanHaveNullValue()
        {
            var rawResult = _ratesService.GetRawData(RatesURI);

            Assert.True(rawResult.Count > 0);

            var normalizeResult = _ratesService.GetNormalizeData(rawResult);

            Assert.True(normalizeResult.Count > 0);

            var result = normalizeResult.GetType().GetProperties()
                .Where(o => o.PropertyType == typeof(string))
                .Select(o => (string)o.GetValue(normalizeResult))
                .Any(value => string.IsNullOrEmpty(value));

            Assert.False(result);
        }
    }
}
